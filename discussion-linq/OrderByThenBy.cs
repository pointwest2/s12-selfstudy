﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion_linq
{
    public class OrderByThenBy
    {
        public void OrderByOperator()
        {
            var courses = CourseDatabase.GetCoursesData();


            var data = courses.OrderBy(c => c.Price);


            foreach (var item in data)
            {
                Console.WriteLine($"{item.Name} - {item.Price} - {item.Author}");
            }

            Console.ReadLine();
        }

        public void ThenByOperator()
        {
            var courses = CourseDatabase.GetCoursesData();

            var data = courses.OrderBy(c => c.Price).ThenByDescending(a => a.Author);


            foreach (var item in data)
            {
                Console.WriteLine($"{item.Name} - {item.Price} - {item.Author}");
            }

        }

    }
}
